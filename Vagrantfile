# -*- mode: ruby -*-
# vi: set ft=ruby :

$download_redis = <<SCRIPT
echo Updating YUM...
sudo yum update -y -q
echo Installing Dependencies...
sudo yum install -y -q make gcc vim wget tcl net-tools telnet ruby rubygems curl unzip
cd ~
echo Download, build and install Redis...
wget -q http://download.redis.io/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
cd redis-stable
make 
sudo echo never > /sys/kernel/mm/transparent_hugepage/enabled
SCRIPT

$configure_redis = <<SCRIPT
sudo cp /root/redis-stable/src/redis-server /usr/local/bin/
sudo cp /root/redis-stable/src/redis-cli /usr/local/bin/
sudo echo never > /sys/kernel/mm/transparent_hugepage/enabled
sudo echo 512 > /proc/sys/net/core/somaxconn
sudo sysctl vm.overcommit_memory=1
mkdir /var/redis /etc/redis
cp /vagrant/redis.conf /etc/redis/6379.conf
mkdir /var/redis/6379
cp /root/redis-stable/utils/redis_init_script /etc/init.d/redis
sudo /etc/init.d/redis start
SCRIPT

$consul_script = <<SCRIPT
echo Fetching Consul...
cd /tmp/
curl -s https://releases.hashicorp.com/consul/0.7.1/consul_0.7.1_linux_amd64.zip -o consul.zip
echo Installing Consul...
unzip consul.zip
sudo chmod +x consul
sudo mv consul /usr/bin/consul
sudo mkdir /etc/consul.d
sudo chmod a+w /etc/consul.d
echo '{"service": {"name": "redis", "port": 6379}}'  |  tee /etc/consul.d/redis.json
SCRIPT

$consul_join = <<SCRIPT
echo Adding Consul client to the server
consul join 172.40.40.101
SCRIPT

# Specify a custom Vagrant box
BOX_NAME = "centos/6"
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo Hello"

  config.vm.define "node_01" do |node_01|
    node_01.vm.box = BOX_NAME
    node_01.vm.network "private_network", ip: "172.40.40.101"
    node_01.vm.hostname = "node01"
    node_01.vm.provision "shell", inline: $download_redis
    node_01.vm.provision "shell", inline: $configure_redis
    node_01.vm.provision "shell", inline: $consul_script
    node_01.vm.provision "shell", inline: "nohup sh -c '/root/redis-stable/src/redis-sentinel /vagrant/sentinel.conf' > /dev/null &"
    node_01.vm.provision "shell", inline: "nohup sh -c 'consul agent -server -bootstrap-expect=2 -data-dir=/tmp/consul -node=agent-1 -bind=172.40.40.101 -config-dir=/etc/consul.d'> /var/log/consul.log &"
  end

  config.vm.define "node_02" do |node_02|
    node_02.vm.box = BOX_NAME
    node_02.vm.network "private_network", ip: "172.40.40.102"
    node_02.vm.hostname = "node02"
    node_02.vm.provision "shell", inline: $download_redis
    node_02.vm.provision "shell", inline: $configure_redis
    node_02.vm.provision "shell", inline: "/root/redis-stable/src/redis-cli -p 6379 slaveof 172.40.40.101 6379"
    node_02.vm.provision "shell", inline: $consul_script
    node_02.vm.provision "shell", inline: "nohup sh -c '/root/redis-stable/src/redis-sentinel /vagrant/sentinel.conf' > /dev/null &"
    node_02.vm.provision "shell", inline: "nohup sh -c 'consul agent -data-dir=/tmp/consul -node=agent-2 -bind=172.40.40.102 -config-dir=/etc/consul.d'> /dev/null &"
    node_02.vm.provision "shell", inline: $consul_join
  end

  config.vm.define "node_03" do |node_03|
    node_03.vm.box = BOX_NAME
    node_03.vm.network "private_network", ip: "172.40.40.103"
    node_03.vm.hostname = "node03"
    node_03.vm.provision "shell", inline: $download_redis
    node_03.vm.provision "shell", inline: $configure_redis
    node_03.vm.provision "shell", inline: "/root/redis-stable/src/redis-cli -p 6379 slaveof 172.40.40.101 6379"
    node_03.vm.provision "shell", inline: $consul_script
    node_03.vm.provision "shell", inline: "nohup sh -c '/root/redis-stable/src/redis-sentinel /vagrant/sentinel.conf' > /dev/null &"
    node_03.vm.provision "shell", inline: "nohup sh -c 'consul agent -data-dir=/tmp/consul -node=agent-3 -bind=172.40.40.103 -config-dir=/etc/consul.d'> /dev/null &"
    node_03.vm.provision "shell", inline: $consul_join
  end
  
end
