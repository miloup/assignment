In this project, I have created 3 Virtual machines using Vagrant with Redis and Consul installed in Cluster mode.
Each Virtual Machine uses Centos-6 box and has 1 Redis + 1 Consul. Redis sentiel is the method used for clustering,
and Consul uses the manual mode (consul join) to avoid using the Atlas account.
The work has been done using Vagrant and some bash scripting. No configuration management tools (Chef, Ansible,.. etc) 
has been used. 

Workflow:
- The only command used to spin up the 3 VMs and have them set up is 'vagrant up' once you clone this project. Please use
  the master branch.
- Once 'vagrant up' triggered, it will create the virtual machines (one by one)  with a static private IP and a hostname.
- After that, Redis will be downloaded from the Redis website, built and its config file will be overwritten with
  the redis.conf file placed in this folder. Redis sentinel will be launched as daemon using 'nohup' and the other members
  will be added automatically by Redis Sentinel.
- In the next step, Consul will be downloaded and installed as follow (1 server and 2 clients, one on each machine).

Test:
- No smoke has been written due to the short amount of time I had to spend learning Vagrant, Redis and Consul. To check the 
  replication, use 'vagrant ssh <some_vm>' and run the following:
  $user : consul members # will display the consul members.
  $user : redis-cli -p 5000 sentinel master mymaster # will display info about the Redis members.

NOTE: This has project has been tested on a MacBook Pro, Intel i-7 2.5GHz, 8-core, 16Gb RAM, and it worked perfectly fine. 
      The time to have the 3 VMs ready is about 20min.


